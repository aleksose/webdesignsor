class TypeWriter {
  constructor(txtElement, words, wait = 3000) {
    this.txtElement = txtElement;
    this.words = words;
    this.txt = "";
    this.wordIndex = 0;
    this.wait = parseInt(wait, 10);
    this.type();
    this.isDeleting = false;
  }

  type() {
    // Current index of word
    const current = this.wordIndex % this.words.length;
    // Get full text of current word
    const fullTxt = this.words[current];

    // Check if deleting
    if (this.isDeleting) {
      // Remove char
      this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
      // Add char
      this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    // Insert txt into element
    this.txtElement.innerHTML = `<span class="txt">${this.txt}</span>`;

    // Initial Type Speed
    let typeSpeed = 190;

    if (this.isDeleting) {
      typeSpeed /= 2;
    }

    // If word is complete
    if (!this.isDeleting && this.txt === fullTxt) {
      // Make pause at end
      typeSpeed = this.wait;
      // Set delete to true
      this.isDeleting = true;
    } else if (this.isDeleting && this.txt === "") {
      this.isDeleting = false;
      // Move to next word
      this.wordIndex++;
      // Pause before start typing
      typeSpeed = 300;
    }

    setTimeout(() => this.type(), typeSpeed);
  }
}

// Init On DOM Load
document.addEventListener("DOMContentLoaded", init);

// Init App
function init() {
  const txtElement = document.querySelector(".txt-type");
  const words = JSON.parse(txtElement.getAttribute("data-words"));
  const wait = txtElement.getAttribute("data-wait");
  // Init TypeWriter
  new TypeWriter(txtElement, words, wait);
}

// Smooth Scolling
$("#main-nav a").on("click", function(event) {
  if (this.hash !== "") {
    event.preventDefault();

    const hash = this.hash;
    $("html, body").animate(
      {
        scrollTop: $(hash).offset().top - 100
      },
      800
    );
  }
});
//Hamburger responsive
function myFunction() {
  var x = document.getElementById("main-nav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}

// function showIphone(){
//   document.getElementById("testbox").style.backgroundColor  = "red";
//   document.getElementById("screen-sizes-img").src="/dist/img/tilbyr/iphone.png";
// }
// function showIphoneLandscape(){
//   document.getElementById("testbox").style.backgroundColor  = "blue";
//   document.getElementById("screen-sizes-img").src="/dist/img/tilbyr/iphone_landscape.png";
// }
// function showIpad(){
//   document.getElementById("testbox").style.backgroundColor  = "green";
//   document.getElementById("screen-sizes-img").src="/dist/img/tilbyr/ipad.png";
// }
// function showIpadLandscape(){
//   document.getElementById("testbox").style.backgroundColor  = "yellow";
//   document.getElementById("screen-sizes-img").src="/dist/img/tilbyr/ipad_landscape.png";
// }
// function showLaptop(){
//   document.getElementById("testbox").style.backgroundColor  = "orange";
//   document.getElementById("screen-sizes-img").src="/dist/img/tilbyr/laptop.png";
// }
// function showPc(){
//   document.getElementById("testbox").style.backgroundColor  = "purple";
//   document.getElementById("screen-sizes-img").src="/dist/img/tilbyr/screen.png";
// }

$(window).on('scroll', function(){
  if($(window).scrollTop()) {
    $('#main-nav-list').addClass('scrolled');
    $('#logo').addClass('scrolled');
  }
  else {
    $('#main-nav-list').removeClass('scrolled');
    $('#logo').removeClass('scrolled');
  }
});